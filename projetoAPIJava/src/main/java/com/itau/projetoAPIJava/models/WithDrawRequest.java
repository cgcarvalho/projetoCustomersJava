package com.itau.projetoAPIJava.models;

public class WithDrawRequest {
	public long customerId;
	public double amount;
	public String currency;
}
