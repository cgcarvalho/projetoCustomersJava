package com.itau.projetoAPIJava.models;

import java.util.HashMap;

public class RatesResponse {
	public String base;
	public String date;
	public HashMap<String, Double> rates;
}
