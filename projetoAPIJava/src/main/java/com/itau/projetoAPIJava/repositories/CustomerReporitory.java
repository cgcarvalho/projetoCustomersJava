package com.itau.projetoAPIJava.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetoAPIJava.models.Customer;

public interface CustomerReporitory extends  CrudRepository<Customer, Long>{
	
	public Customer findByUsername(String username);

}
