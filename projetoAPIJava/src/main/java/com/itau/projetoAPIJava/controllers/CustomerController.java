package com.itau.projetoAPIJava.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.projetoAPIJava.models.Customer;
import com.itau.projetoAPIJava.models.WithDrawRequest;
import com.itau.projetoAPIJava.repositories.CustomerReporitory;
import com.itau.projetoAPIJava.services.CustomerService;
import com.itau.projetoAPIJava.services.RatesConverterService;

@RestController
public class CustomerController {
	
	@Autowired
	CustomerReporitory customerRepository;
	
	RatesConverterService serviceRatesConverter = new RatesConverterService();
	CustomerService serviceCustomer = new CustomerService();

	@RequestMapping(method=RequestMethod.PUT, path="/customer/withdraw")
	public ResponseEntity<Customer> withDraw(@Valid @RequestBody WithDrawRequest withDrawRequest) {
		Optional<Customer> optionalCustomer = customerRepository.findById(withDrawRequest.customerId);
		if(!optionalCustomer.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Customer customerBanco = optionalCustomer.get();
		
		double convertedValue = serviceRatesConverter.convert(withDrawRequest.amount, withDrawRequest.currency);
		double oldBalance = customerBanco.getBalance();
		
		customerBanco.setBalance(serviceCustomer.withDraw(oldBalance, convertedValue));
		Customer customerSalvo = customerRepository.save(customerBanco);
		
		return ResponseEntity.ok().body(customerSalvo);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/customer")
	public Customer addCustomer(@Valid @RequestBody Customer customer) {

		return customerRepository.save(customer);
	}
	
	
}
