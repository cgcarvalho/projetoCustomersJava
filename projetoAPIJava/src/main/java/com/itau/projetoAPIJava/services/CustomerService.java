package com.itau.projetoAPIJava.services;

import org.springframework.stereotype.Service;

@Service
public class CustomerService {

	public double withDraw(double oldBalance, double withDrawValue) {
		
		return oldBalance - withDrawValue;
	}
}
