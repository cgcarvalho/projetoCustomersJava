package com.itau.projetoAPIJava.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.projetoAPIJava.models.RatesResponse;

@Service
public class RatesConverterService {
	
	RestTemplate service = new RestTemplate();
	final String key = "e539e11f4de63138df2cea4a75a4ad58";
	final String urlService = "http://data.fixer.io/api/latest";
	
	public double convert(double convertValue, String currency) {
		
		RatesResponse ratesResponse = getRates(currency);
		double convertedValue = 0;
		if(ratesResponse.rates.containsKey(currency)) {
			convertedValue = convertValue / ratesResponse.rates.get(currency);
		}

		return convertedValue;
	}
	
	private RatesResponse getRates(String currency) {
		String url = urlService + "?access_key=" + key + "&base=EUR&symbols=" + currency;
				
		return service.getForObject(url, RatesResponse.class);
	}
}
